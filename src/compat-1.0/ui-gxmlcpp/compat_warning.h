#ifndef UI_GXML_COMPAT_WARNING_H
#define UI_GXML_COMPAT_WARNING_H

#ifndef UI_GXMLCPP_IGNORE_DEPRECATED
#ifdef WIN32
#pragma message("DEPRECATED API: XMLDump.hpp XMLNode.hpp XMLNodeSet.hpp XMLTree.hpp XSLTrans.hpp")
#else
#warning DEPRECATED API: XMLDump.hpp XMLNode.hpp XMLNodeSet.hpp XMLTree.hpp XSLTrans.hpp
#endif
#endif

#endif
