/*
 * (C) Copyright 2002-2003, Schlund+Partner AG
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/**
 * @file XSLTrans.hpp
 * @version @$Id$
 * @author Schlund + Partner AG
 * @brief *ps*
 *
 * (C) Copyright by Schlund+Partner AG
 *
 * Synopsis: @#include <ui-gxmlcpp/XSLTrans.hpp>
 *
 */

#ifndef UI_GXML_XSLTRANS_HPP
#define UI_GXML_XSLTRANS_HPP

// THIS IS A COMPAT API ONLY
#include <ui-gxmlcpp/compat_warning.h>

// STDC++
#include <string>

// C++ Libraries
#include <ui-utilcpp/auto_ptr_compat.hpp>
#include <ui-gxmlcpp/XMLDump.hpp>
#include <ui-gxmlcpp/XMLTree.hpp>

// C libraries
#include <libxml/xpath.h>
#include <libxml/tree.h>
#include <libxslt/xsltInternals.h>

namespace UI {
namespace GXML {

class XSLTrans
{
public:
	/** @brief Error codes for exceptions. */
	enum ErrorCode
	{
		Parse_=1,  // Stylesheet not parsed (not well formed XML)
		Style_,    // Not a valid stylesheet
		Trans_     // Transforming error
	};
	/** @brief Exceptions for this class. */
	typedef CodeException<ErrorCode> Exception;

	XSLTrans(char const * xmlBuffer, int size=-1, std::string const & baseURI="");
	XSLTrans(std::string const & xmlString, std::string const & baseURI="");
	XSLTrans(std::ifstream & f, std::string const & baseURI="");

	~XSLTrans();

	xsltStylesheetPtr getStylesheetPtr();

	// Trans into XMLTree
	UI::Util::auto_ptr<XMLTree> transToXMLTree(xmlDocPtr const doc) const;

	UI::Util::auto_ptr<XMLDump> trans(xmlDocPtr const doc) const;
	UI::Util::auto_ptr<XMLDump> trans(XMLTree const * xmlTree) const;
	UI::Util::auto_ptr<XMLDump> trans(std::string const & xmlString) const;

private:
	xmlDocPtr _doc;
	xsltStylesheetPtr _style;

	void genTrans(char const * xmlBuffer, int size=-1, std::string const & baseURI="");

	/** @bug This is not used (?!). */
	static char const * _defaultEncoding;
};

}}
#endif
