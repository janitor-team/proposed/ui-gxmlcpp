/*
 * (C) Copyright 2002, Schlund+Partner AG
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/**
 * @file XMLDump.hpp
 * @version @$Id$
 * @author Schlund + Partner AG
 * @brief *ps*
 *
 * (C) Copyright by Schlund+Partner AG
 *
 * Synopsis: @#include <ui-gxmlcpp/XMLDump.hpp>
 *
 * Serializing XML Trees.
 *
 */

#ifndef UI_GXML_XMLDUMP_HPP
#define UI_GXML_XMLDUMP_HPP

// THIS IS A COMPAT API ONLY
#include <ui-gxmlcpp/compat_warning.h>

// STDC++
#include <string>

// C libraries
#include <libxslt/xsltInternals.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/tree.h>
#include <libxslt/transform.h>
#include <libxslt/xsltutils.h>
#include <libxslt/imports.h>

// Local
#include <ui-gxmlcpp/Util.hpp>
#include <ui-gxmlcpp/XMLTree.hpp>

namespace UI
{
namespace GXML
{

// toDo
class XSLTOutputBuf
{
public:
	/** @brief Error codes for exceptions. */
	enum ErrorCode
	{
		Mem_,                // Mem Error
		ResultInvalid_,      // Result Doc invalid (0 or no children)
		StyleInvalid_        // Style invalid (0)
	};
	/** @brief Exceptions for this class. */
	typedef CodeException<ErrorCode> Exception;

	XSLTOutputBuf(xmlDocPtr result, xsltStylesheetPtr style);
	~XSLTOutputBuf();
	char const * getAddr() const;
	int getSize() const;

private:
	xmlCharEncodingHandlerPtr _encoder;
	xmlOutputBufferPtr _buf;
};

class XMLDump
{
public:
	/** @brief Error codes for exceptions. */
	enum ErrorCode
	{
		Mem_,           // XML could not be parsed
		NodeNotFound_   // Node could not be found from path
	};
	/** @brief Exceptions for this class. */
	typedef CodeException<ErrorCode> Exception;

	XMLDump(xmlDocPtr const doc, const xmlXPathContextPtr ctxt, const std::string & path="/");
	XMLDump(XMLTree const * const tree, const std::string & path="/");
	XMLDump(xmlDocPtr const doc);
	// toDo
	XMLDump(XSLTOutputBuf const * buf);

	~XMLDump();

	char const * getAddr() const;
	std::string get() const;
	int getSize() const;

private:
	xmlBufferPtr _nodeDump;           // This is used to dump XML from some path other than "/"
	std::pair<xmlChar *, int> _docDump;    // This is used to dump the whole XML document (path = "/").
	// toDo
	XSLTOutputBuf * _xsltDump;        // This is used when dumping was actually done outside. Currently only for XSL Transformations.

	// genDump: Master Create Dump Function. Must be used in Constructors only
	void genDump(xmlDocPtr const doc, xmlXPathContextPtr const ctxt, std::string const & path);

	static char const * const _nullString;
};

}}
#endif
