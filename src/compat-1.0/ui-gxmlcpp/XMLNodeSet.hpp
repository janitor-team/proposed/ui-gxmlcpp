/*
 * (C) Copyright 2002, Schlund+Partner AG
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/**
 * @file XMLNodeSet.hpp
 * @version @$Id$
 * @author Schlund + Partner AG
 * @brief *ps*
 *
 * (C) Copyright by Schlund+Partner AG
 *
 * Synopsis: @#include <ui-gxmlcpp/XMLNodeSet.hpp>
 *
 */

#ifndef UI_GXML_XMLNODESET_HPP
#define UI_GXML_XMLNODESET_HPP

// THIS IS A COMPAT API ONLY
#include <ui-gxmlcpp/compat_warning.h>

// C libraries
#include <libxslt/xsltInternals.h>
#include <libxml/xpath.h>
#include <libxml/tree.h>

// Local
#include <ui-gxmlcpp/XMLNode.hpp>
#include <ui-gxmlcpp/Exception.hpp>

namespace UI {
namespace GXML {

// forward declarations
class XMLTree;
/**
 * @bug XMLNodeSet should deliver an empty list if there are no nodes, should _not_ throw exception.
 *
 */
class XMLNodeSet
{
public:
	/** @brief Error codes for exceptions. */
	enum ErrorCode
	{
		Eval_ = 0,		// Could not evaluate XPath
		NoSet_ = 1,		// XPath delivers no node set
		NoMatch_ = 2		// XPath doesn't exist
	};
	/** @brief Exceptions for this class. */
	typedef CodeException<ErrorCode> Exception;

	XMLNodeSet(std::string const & xpath, XMLTree const * tree);
	~XMLNodeSet();

	class Iterator
	{
	public:
		Iterator();
		Iterator(xmlNodeSetPtr const set);
		~Iterator();
		Iterator operator++();
		bool operator!=(Iterator const & iter) const;
		XMLNode operator*() const;
		int getPosition() const;

	private:
		int _pos;
		xmlNodeSetPtr _set;
	};

	int size() const;
	Iterator begin() const;
	Iterator end() const;
	//Iterator rbegin();
	//Iterator rend();

private:
	xmlXPathObjectPtr _xPathObj;
};

}}
#endif
