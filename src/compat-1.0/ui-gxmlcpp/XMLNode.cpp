/*
 * (C) Copyright 2002, Schlund+Partner AG
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// Local configuration
#include "config.h"

// Implementation
#include "XMLNode.hpp"

// STDC
#include <cassert>
#include <cstring>

// C++ libaries
#include <ui-utilcpp/Text.hpp>

// Local
#include "../../src/ui-gxmlcpp/Util.hpp"


namespace UI {
namespace GXML {

XMLNode::XMLNode()
	:_node(0)
{}

XMLNode::XMLNode(xmlNodePtr const node)
	:_node(node)
{
	if (_node == 0)
	{
		UI_THROW_CODE(NoNode_, "Node pointer is null");
	}
}

XMLNode::~XMLNode()
{}

int XMLNode::getType() const
{
	return _node->type;
}

XMLNode XMLNode::getNext() const
{
	if (_node->next == 0)
	{
		UI_THROW_CODE(LastNode_, "Node has no following sibling");
	}
	assert(_node->next);
	XMLNode result(_node->next );
	return result;
}

XMLNode XMLNode::getPrev() const
{
	if (_node->prev == 0)
	{
		UI_THROW_CODE(FirstNode_, "Node has no preceding sibling");
	}
	assert(_node->prev);
	XMLNode result(_node->prev);
	return result;
}

XMLNode XMLNode::getParent() const
{
	if (_node->parent == 0)
	{
		UI_THROW_CODE(NoParent_, "Node has no parent node");
	}
	assert(_node->parent);
	XMLNode result(_node->parent);
	return result;
}

XMLNode XMLNode::getLast() const
{
	if (_node->last == 0)
	{
		// _node->last points to the last child of the node
		UI_THROW_CODE(NoChild_, "Node has no child node");
	}
	assert(_node->last);
	XMLNode result(_node->last);
	return result;
}

XMLNode XMLNode::getChild() const
{
	if (_node->children == 0)
	{
		// should only happen if the node has no content AND no child nodes.
		UI_THROW_CODE(NoChild_, "Node has no child node");
	}
	assert(_node->children);
	XMLNode result(_node->children);
	return result;
}

std::string XMLNode::getName() const
{
	// node with no name should not exist
	assert(_node->name);
	return (std::string)(char*)_node->name;
}

std::string XMLNode::getContent() const
{
	if (_node->children == 0)
	{
		UI_THROW_CODE(NoChild_, "Node has no child node");
	}
	assert(_node->children);
	// if node has content it will be the content of the first child (text-node)
	if (_node->children->content == 0)
	{
		UI_THROW_CODE(NoChild_, "Node contains no text");
	}
	assert(_node->children->content);
	return (std::string)(char*)_node->children->content;
}


std::string XMLNode::getAttribute(std::string const & name) const
{
	if (_node->properties == 0)
	{
		UI_THROW_CODE(NoAttribute_, "Node has no attributes");
	}

	// search for attribute by 'name'
	xmlAttrPtr attr(_node->properties);

	while (attr != 0 && std::strcmp((char *)attr->name, name.c_str()) != 0)
	{
		attr = attr->next;
	}

	if (!attr)
	{
		UI_THROW_CODE(NoAttribute_, "Node has no auch attribute: " + name);
	}

	// Attribute found
	assert(attr->children);
	if (attr->children->content == 0)
	{
		// will never happen, cause child of attribute is always text-node (although it might be empty)
		UI_THROW_CODE(NoContent_, "Node contains no text");
	}
	assert(attr->children->content);
	return (char *) attr->children->content;
}

std::string XMLNode::getNodeDump() const
{
	xmlBufferPtr nodeDump = xmlBufferCreate();
	if (!nodeDump)
	{
		UI_THROW_CODE(BufferCreate_, "xmlBufferCreate: Allocation error");
	}
	if (_node)
	{
		xmlNodeDump(nodeDump, _node->doc, _node, 0, 0);
		if (!nodeDump->content)
		{
			xmlBufferFree(nodeDump);
			UI_THROW_CODE(NodeDump_, "Could not dump node");
		}
	}
	else
	{
		UI_THROW_CODE(NodeDump_, "Could not dump node");
	}

	std::string result((char *) nodeDump->content);
	xmlBufferFree(nodeDump);
	return result;
}

xmlNodePtr XMLNode::getNodePtr() const
{
	return _node;
}

}}
