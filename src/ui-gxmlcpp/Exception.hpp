/**
 * @file
 * @version @$Id$
 * @author
 * ui-gxmlcpp: C++ wrapper for Gnome libxml2/libxsl.
 * Copyright (C) Schlund+Partner AG 2002-2004.
 * Licensed under the GNU Lesser General Public License (LGPL). See
 * @ref License, or the file COPYING in distribution.
 */
#ifndef UI_GXML_EXCEPTION_HPP
#define UI_GXML_EXCEPTION_HPP

// STDC++
#include <string>

// C++ libraries
#include <ui-utilcpp/Exception.hpp>

namespace UI {
namespace GXML {

/** @brief Mother exception class for this namespace. */
class Exception: public UI::Exception
{
public:
	Exception(std::string const & what=NoWhatGiven_, std::string const & debug=NoDebugGiven_)
		:UI::Exception(what, debug)
	{}
	virtual ~Exception() throw() {};
};

/** @brief Adding code facility to Exception. */
template <typename Code = int>
class CodeException: public Exception
{
public:
	/** @brief Construct with code and description text. */
	CodeException(Code const & code, std::string const & what=NoWhatGiven_, std::string const & debug=NoDebugGiven_)
		:UI::GXML::Exception(what, debug)
		,code_(code)
	{};

	/** @brief Get code. */
	Code const & getCode() const { return code_; }

private:
	Code const code_;
};

}}
#endif
