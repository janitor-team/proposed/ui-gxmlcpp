/**
 * @file
 * @version @$Id$
 * @author
 * ui-gxmlcpp: C++ wrapper for Gnome libxml2/libxsl.
 * Copyright (C) Schlund+Partner AG 2002-2004.
 * Licensed under the GNU Lesser General Public License (LGPL). See
 * @ref License, or the file COPYING in distribution.
 */

// Local configuration
#include "config.h"

// Implementation
#include "XSLTree.hpp"

// STDC++
#include <fstream>
#include <cassert>

// C libraries
#include <libxslt/xsltInternals.h>
#include <libxslt/transform.h>
#include <libxslt/xsltutils.h>

// C++ libraries
#include <ui-utilcpp/Text.hpp>

// Local
#include "Util.hpp"

namespace UI {
namespace GXML {

// Private helper function for constructors
xsltStylesheetPtr XSLTree::create(xmlDocPtr const doc)
{
	xsltStylesheetPtr style;
	style = xsltParseStylesheetDoc(doc);
	if (!style)
	{
		UI_THROW_CODE(Style_, "getTrans: Stylesheet not valid");
	}
	return style;
}

XSLTree::XSLTree(char const * xml, int len, std::string const & base, int const options)
	:Tree(xml, len, base, XSLT_PARSE_OPTIONS | options)
	,style_(create(get()))
{}

XSLTree::XSLTree(std::string const & xml, std::string const & base, int const options)
	:Tree(xml, base, XSLT_PARSE_OPTIONS | options)
	,style_(create(get()))
{}

XSLTree::XSLTree(std::istream & xml, std::string const & base, int const options)
	:Tree(xml, base, XSLT_PARSE_OPTIONS | options)
	,style_(create(get()))
{}

XSLTree::XSLTree(FileConstructor const dummy, std::string const & file, int const options)
	:Tree(dummy, file, XSLT_PARSE_OPTIONS | options)
	,style_(create(get()))
{}

XSLTree::XSLTree(XSLTree const & tree)
	:Tree(tree)
{
	throw UI::Exception("not implemented");
}

XSLTree & XSLTree::operator=(XSLTree const &)
{
	throw UI::Exception("not implemented");
}

XSLTree::~XSLTree()
{
	xsltFreeStylesheet(style_);
	/** @note xsltFreeStylesheet frees underlying xmlDoc as well, so we
	 * need to disable calling xmlFreeDoc in ~Tree. */
	setDontFreeDoc();
}

std::string XSLTree::getOutputEncoding() const
{
	return style_->encoding ? (char *) style_->encoding : UI::Util::EmptyString_;
}

xsltStylesheet * XSLTree::getStylesheet() const
{
	return style_;
}

}}
