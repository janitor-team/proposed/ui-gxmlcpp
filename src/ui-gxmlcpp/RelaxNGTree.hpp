/**
 * @file
 * @version @$Id$
 * @author
 * ui-gxmlcpp: C++ wrapper for Gnome libxml2/libxsl.
 * Copyright (C) Schlund+Partner AG 2002-2004.
 * Licensed under the GNU Lesser General Public License (LGPL). See
 * @ref License, or the file COPYING in distribution.
 */
#ifndef UI_GXML_RELAXNGTREE_HPP
#define UI_GXML_RELAXNGTREE_HPP

// STDC++
#include <string>

// C++ Libraries
#include <ui-gxmlcpp/ValidatorTree.hpp>

// C libraries
#include <libxml/relaxng.h>

namespace UI {
namespace GXML {

/** @brief RelaxNG Schema XML Tree. */
class RelaxNGTree: public ValidatorTree
{
private:
	void init();

public:
	/** @name Standard tree constructors.
	 * @{ @see UI::GXML::Tree. */
	RelaxNGTree(char const * xml, int len=-1, std::string const & base=DefaultDocbase_, int const options=0);
	RelaxNGTree(std::string const & xml, std::string const & base=DefaultDocbase_, int const options=0);
	RelaxNGTree(std::istream & xml, std::string const & base=DefaultDocbase_, int const options=0);
	RelaxNGTree(FileConstructor const dummy, std::string const & file, int const options=0);
	/** @} */

public:
	~RelaxNGTree();

private:
	virtual int libxml2Validate(Tree const & tree) const;

	xmlRelaxNGParserCtxt * parserCtxt_;
	xmlRelaxNG * relaxng_;
};

}}
#endif
