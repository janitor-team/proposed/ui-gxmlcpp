/**
 * @file
 * @version @$Id$
 * @author
 * ui-gxmlcpp: C++ wrapper for Gnome libxml2/libxsl.
 * Copyright (C) Schlund+Partner AG 2002-2004.
 * Licensed under the GNU Lesser General Public License (LGPL). See
 * @ref License, or the file COPYING in distribution.
 */
#ifndef UI_GXML_BUFFER_HPP
#define UI_GXML_BUFFER_HPP

// STDC++
#include <string>

// C libraries
#include <libxml/tree.h>

// C++ libraries
#include <ui-gxmlcpp/Exception.hpp>
#include <ui-gxmlcpp/Util.hpp>

namespace UI {
namespace GXML {

/** @brief Abstraction for libxml2's xmlBuffer. */
class Buffer: private UI::Util::auto_base<xmlBuffer>
{
public:
	/** @brief Error codes for exceptions. */
	enum ErrorCode
	{
		Mem_
	};
	/** @brief Exceptions for this class. */
	typedef CodeException<ErrorCode> Exception;

	Buffer();
	~Buffer();

	/** @brief Append text. */
	void concat(std::string const & text);
	/** @brief Get buffer content address. */
	char const * getBuf() const;
	/** @brief Get buffer content size. */
	int getSize() const;

	using UI::Util::auto_base<xmlBuffer>::get;
};

/** @brief OutputBuffer holder class. */
class OutputBuffer: private Buffer
{
public:
	OutputBuffer();

	using Buffer::ErrorCode;
	using Buffer::Exception;
	using Buffer::concat;
#ifdef LIBXML2_NEW_BUFFER
	~OutputBuffer();
	/** @brief Get output buffer content. */
	char const * getBuf() const;
	/** @brief Get output buffer content size. */
	int getSize() const;
#else
	using Buffer::getBuf;
	using Buffer::getSize;
#endif

	/** @brief Get underlying pointer. */
	xmlOutputBuffer * get() const;
	/** @brief Smart dereferencing. */
	xmlOutputBuffer * operator->() const;

private:
#ifdef LIBXML2_NEW_BUFFER
	xmlOutputBuffer * outbuf_;
#else
	xmlOutputBuffer outbuf_;
#endif
};

}}
#endif
