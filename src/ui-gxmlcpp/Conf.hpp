/**
 * @file
 * @version @$Id$
 * @author
 * ui-gxmlcpp: C++ wrapper for Gnome libxml2/libxsl.
 * Copyright (C) Schlund+Partner AG 2002-2004.
 * Licensed under the GNU Lesser General Public License (LGPL). See
 * @ref License, or the file COPYING in distribution.
 */
#ifndef UI_GXML_CONF_HPP
#define UI_GXML_CONF_HPP

// STDC++
#include <string>

namespace UI {
namespace GXML {

/** @brief libxml2/libxslt configuration helper.
 *
 * You would normally have one instance of this alive while
 * operational. For custom logging, overwrite customLog().
 *
 */
class Conf
{
public:
	Conf();
	virtual ~Conf();

	/** @brief Overwrite this for custom logging.
	 *
	 * This defaults to logging to std::clog.
	 *
	 * @param msg Fragmented log stream including newlines.
	 *
	 * If you have line based logging, you might consider streaming msg
	 * into a stream buffer (with multi thread safety) and log only when
	 * you got a complete line. Unfortunately, msg may or may not
	 * include newlines and/or be a complete log line or not (i.e.,
	 * lines are being broadcast to the hooks in fragments).
	 */
	virtual void customLog(std::string const & msg);

	/** @brief Set/unset custom logging (optionally with custom prefix).
	 *
	 * When enabled, logging will be done through us (i.e.,
	 * libxmlLog()). You would normally want to call this in a derived
	 * class' constructor where customLog() is overwritten.
	 */
	Conf & setCustomLogging(bool on=true, std::string prefix="[libxml2/xslt] ");
	/** @brief En/disable custom logging. Defaults to "enable" ("don't care" if custom logging is unset). */
	Conf & setCustomLoggingEnable(bool on=true);
	/** @brief Odd libxml2 global feature ;). See libxml2 docs. */
	static void setKeepBlanks(bool on=true);
	/** @brief Register exslt functions for use with xsl transformations. */
	Conf & setEXSLT();

private:
	void memberLog(std::string const & msg);
	static void libxmlLog(void * ctx, const char * msg, ...);
	bool customLoggingEnabled_;
	std::string logPrefix_;
};

}}
#endif
