/** @file */

// Local configuration
#include "config.h"

// Implementation
#include "ValidatorTree.hpp"

namespace UI {
namespace GXML {

ValidatorTree::ValidatorTree(char const * xml, int len, std::string const & base, int const options): Tree(xml, len, base, options) {}
ValidatorTree::ValidatorTree(std::string const & xml, std::string const & base, int const options): Tree(xml, base, options) {}
ValidatorTree::ValidatorTree(std::istream & xml, std::string const & base, int const options): Tree(xml, base, options) {}
ValidatorTree::ValidatorTree(FileConstructor const dummy, std::string const & file, int const options): Tree(dummy, file, options) {}

// Not implemented
	ValidatorTree::ValidatorTree(ValidatorTree const & ): Tree()                    { throw UI::Exception("not implemented"); }
	ValidatorTree & ValidatorTree::operator=(ValidatorTree const & ) { throw UI::Exception("not implemented"); }

ValidatorTree::~ValidatorTree() {}

bool ValidatorTree::isValid(Tree const & tree) const
{
	int const result(libxml2Validate(tree));
	if (result < 0)
	{
		UI_THROW_CODE(Internal_, "Internal libxml2 error in validation");
	}
	return result == 0;
}

Tree const & ValidatorTree::validate(Tree const & tree) const
{
	if (!isValid(tree))
	{
		UI_THROW_CODE(TreeInvalid_, "Tree invalid: " + tree.dump());
	}
	return tree;
}

}}
