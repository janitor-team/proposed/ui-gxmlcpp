/**
 * @file
 * @version @$Id$
 * @author
 * ui-gxmlcpp: C++ wrapper for Gnome libxml2/libxsl.
 * Copyright (C) Schlund+Partner AG 2002-2004.
 * Licensed under the GNU Lesser General Public License (LGPL). See
 * @ref License, or the file COPYING in distribution.
 */
#ifndef UI_GXML_XPATHCONTEXT_HPP
#define UI_GXML_XPATHCONTEXT_HPP

// STDC++
#include <string>

// C libraries
#include <libxml/tree.h>
#include <libxml/xpath.h>

// C++ libraries
#include <ui-gxmlcpp/Exception.hpp>
#include <ui-gxmlcpp/Util.hpp>

namespace UI {
namespace GXML {

class Tree;

/** @brief XPath context holder class.
 *
 * @note xpath context associated to xmlDocs are not constant, and cannot be shared between threads.
 *
 */
class XPathContext: private UI::Util::auto_base<xmlXPathContext>
{
	friend class Tree;
	friend class XPathObject;

	// Compatibility
	friend class XMLTree;
	friend class XMLDump;
	friend class XMLNodeSet;

private:
	/** Constructor helper. */
	static xmlXPathContext * create(xmlDoc * doc, xmlNode * node);

public:
	/** @brief Error codes for exceptions. */
	enum ErrorCode
	{
		Create_             // Could not create (XPath) context for Tree
	};
	/** @brief Exceptions for this class. */
	typedef CodeException<ErrorCode> Exception;

//protected:
	/** @brief Constructor libxml2 doc. */
	XPathContext(xmlDoc * doc, xmlNode * node=0);

public:
	/** @brief Constructor from Tree. */
	XPathContext(Tree const & tree, xmlNode * node=0);
	~XPathContext();

	/** @brief Register a new namespace prefix for this context. */
	void registerNamespace(std::string const & prefix, std::string const & uri);
};

}}
#endif
