/**
 * @file
 * @version @$Id$
 * @author
 * ui-gxmlcpp: C++ wrapper for Gnome libxml2/libxsl.
 * Copyright (C) Schlund+Partner AG 2002-2004.
 * Licensed under the GNU Lesser General Public License (LGPL). See
 * @ref License, or the file COPYING in distribution.
 */

// Local configuration
#include "config.h"

// STDC++
#include <iostream>
#include <cassert>

// C++ libraries
#include <ui-gxmlcpp/Conf.hpp>
#include <ui-gxmlcpp/Tree.hpp>
#include <ui-gxmlcpp/XSLTree.hpp>
#include <ui-gxmlcpp/XSLTransTree.hpp>

int main(int argc, char** argv)
{
	try
	{
		if (argc < 3)
		{
			std::cerr << "Usage: " << argv[0] << " XSL-FILE XML-FILE" << std::endl;
			exit(1);
		}

		// libxml2/xslt global configuration
		UI::GXML::Conf conf;
		conf.setEXSLT();

		// Parse xsl and xml trees
		UI::GXML::XSLTree xslTree(UI::GXML::Tree::File_, argv[1]);
		UI::GXML::Tree xmlTree(UI::GXML::Tree::File_, argv[2]);

		// Get xsl translation result (with parameter example; this program should be updated to accept params via arguments)
		UI::GXML::XSLTransTree xslTransTree(xslTree, xmlTree, UI::GXML::XSLTransTree::Params().add("example_only_param1", "value1").add("example_only_param2", "value2"));

		// Get xsl translation result dump
		// Note: For simplicity (i.e., if you don't need to hold/cache/work
		// with the result) you could also simply use Tree::dump())
		UI::GXML::XSLTransTree::Dump dump(xslTransTree);

		// Output xsl translation dump
		std::cerr << "Encoding is: " << dump.getEncoding() << std::endl;
		std::cout << dump.get();
	}
	catch (std::exception const & e)
	{
		std::cerr << "Error: " << e.what() << std::endl;
	}
}
